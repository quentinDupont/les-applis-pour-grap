---
title: "Les applis pour Grap"
order: 0
in_menu: true
---
J'écris ici un texte de présentation de mon mini-site Framalibre et je peux
lister les logiciels libres que je recommande.

<h1> Les logiciels internes </h1>


<article class="framalibre-notice">
  <div>
    <img src="https://beta.framalibre.org/images/logo/PeerTube.png">
  </div>
  <div>
    <h2>Odoo</h2>
    <p>Votre logiciel de gestion.</p>
    <div>
      <a href="https://erp.grap.coop">Vers le site</a>
    </div>
  </div>
</article> 

<h1> Les logiciels en plus  </h1>

<article class="framalibre-notice">
  <div>
    <img src="https://beta.framalibre.org/images/logo/PeerTube.png">
  </div>
  <div>
    <h2>PeerTube</h2>
    <p>PeerTube est un logiciel décentralisé et fédéré d'hébergement de vidéos.</p>
    <div>
      <a href="https://beta.framalibre.org/notices/peertube.html">Vers la notice Framalibre</a>
      <a href="https://joinpeertube.org/fr/">Vers le site</a>
    </div>
  </div>
</article> 

  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/CryptPad.png">
    </div>
    <div>
      <h2>CryptPad</h2>
      <p>Avec CryptPad, vous pouvez créer des documents collaboratifs rapidement pour prendre des notes à plusieurs.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/cryptpad.html">Vers la notice Framalibre</a>
        <a href="https://cryptpad.fr">Vers le site</a>
      </div>
    </div>
  </article> 